#!/usr/bin/python3

import subprocess, os, sys, gettext, json
from locale import getlocale
from time import sleep

# Translation
path = os.path.dirname(os.path.realpath(__file__))
translations = dict()
supported_langs = {
    'en_US' : 'en',
    'ru_RU' : 'ru'
}
for locale, lang in zip(supported_langs.keys(), supported_langs.values()):
    translations[locale] = gettext.translation('messages', localedir = path + '/data', languages=[lang])
try:
    translations[getlocale()[0]].install()
except KeyError:
    translations['en_US'].install()

def inputbox(text, default=''):
        dialog = subprocess.run(['kdialog', '--inputbox', text , default,'--title','WebM Script'], capture_output=True, encoding='UTF-8', check=True)
        return dialog.stdout.strip('\n')

def readqueue():
    global queue
    try:
        with open('webm', 'r') as f:  
            queue = json.load(f)
    except FileNotFoundError:
        pass

def writequeue():
    global queue
    try:
        with open('webm','x') as f:
            queue = [os.path.basename(output_file)]
            json.dump(queue,f)
    except FileExistsError:
        readqueue()
        queue.append(os.path.basename(output_file))
        with open('webm', 'w') as f:
            json.dump(queue, f)

def removequeue():
    readqueue()
    queue.remove(os.path.basename(output_file))
    with open('webm', 'w') as f:
          json.dump(queue, f)
            
def notification(text, icon, notify_id=None):
    nargs = ['notify-send']
    if (icon == 'dialog-information' or icon == 'dialog-question'):
        nargs.extend(('-t','0','-p'))
    else:
        nargs.extend(('-u','normal'))
    if notify_id is not None:
        nargs.extend(('-r',notify_id))
    nargs.extend(('--icon',icon,'--hint=string:desktop-entry:org.kde.dolphin', '--app-name=WebM Script',text))
    ntrun = subprocess.run(nargs, capture_output=True,encoding='UTF-8')
    return ntrun.stdout.strip('\n')

def run_conversion(args): 
    global queue
    # Conversion order
    # queue = list()
    try:
        queue_size = int(os.environ['WEBM'])
    except KeyError:
        queue_size = 1

    writequeue()

    notify_id = None
    if os.path.basename(output_file) not in queue[:queue_size]:
        notify_id = notification(_('Файл {0} ожидает конверсии').format(os.path.basename(output_file)),'dialog-question',notify_id)
        while(os.path.basename(output_file) not in queue[:queue_size]):
            sleep(1)
            readqueue()
    notify_id = notification(_('Идёт конверсия файла {0}').format(os.path.basename(output_file)),'dialog-information',notify_id)

    try:
        for arg in args:
            subprocess.run(arg,check=True)
    except subprocess.CalledProcessError:
        notification(_('Конверсия файла {0} завершилась с ошибкой!').format(os.path.basename(output_file)), 'dialog-error', notify_id)
    else:
        notification(_('Конверсия файла {0} завершена!').format(os.path.basename(output_file)), 'dialog-positive', notify_id)

    removequeue()

queue = list()

if __name__ == "__main__":
    input_file=os.path.abspath(os.path.expanduser(sys.argv[1]))
    os.chdir('/tmp')
    output_file = os.path.splitext(input_file)[0] + '.webm'
    readqueue()
    if os.path.exists(output_file) or os.path.basename(output_file) in queue:
        number = 1
        new_output_file = output_file.replace('.webm', '_{0}.webm'.format(number))
        while(os.path.exists(new_output_file) or os.path.basename(new_output_file) in queue):
            number += 1
            new_output_file = output_file.replace('.webm', '_{0}.webm'.format(number))
        output_file = new_output_file
    start = inputbox(_('Выберите время начала'))
    duration = inputbox(_('Выберите продолжительность видео'))
    bitrate = 4 * 1024 * 0.99 / int(duration) * 8
    # 4chan max resolution 2048x2048
    command = f"ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of default=noprint_wrappers=1:nokey=1 \"{input_file}\""
    probe = subprocess.run(command, shell=True, capture_output=True, encoding='UTF-8')
    if probe.returncode:
        notification(_('Конверсия файла {0} завершилась с ошибкой!').format(os.path.basename(output_file)), 'dialog-error')
        print(command)
        raise SystemExit('ffprobe has failed!')
    resolution=list(map(int,probe.stdout.splitlines()))
    if any(dimension > 2048 for dimension in resolution):
        if resolution[0] > resolution[1]:
            scale = 'scale=1920:-1'
        else:
            scale = 'scale=-1:1920'
    else:
        scale = "scale=0:0"
    args = [[
            'ffmpeg', '-ss', start, '-i', input_file, '-t', duration,
            '-c:v', 'vp9', '-b:v', str(bitrate)+'k', '-an',
            '-vf', scale, '-pass', '1', '-passlogfile', os.path.basename(output_file), '-f', 'null', '/dev/null'
        ],[
            'ffmpeg', '-ss', start, '-i', input_file, '-t', duration,
            '-c:v', 'vp9', '-b:v', str(bitrate)+'k', '-an',
            '-vf', scale, '-pass', '2','-passlogfile', os.path.basename(output_file), '-fs', '4M', output_file
        ]]
    run_conversion(args)