��          �                      +   -     Y  (   r  %   �  $   �  >   �     %     ?  *   Y  5   �  K   �            .   "  �  Q     �     �     
          ,     B      Y     z  	   �     �     �  )   �     �     �  "      Битрейт: Введите битрейт (300k/3M/...) Входной файл: Выберите время начала Выберите имиджборду Выберите пресет (0-13) Выберите продолжительность видео Выходной файл Длительность: Идёт конверсия файла {0} Конверсия файла {0} завершена! Конверсия файла {0} завершилась с ошибкой! Начало: Пресет: Файл {0} ожидает конверсии Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2023-10-29 18:54+0300
PO-Revision-Date: 2023-10-22 19:07+0300
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.12.1
 Bitrate: Enter the bitrate (300k/3M/...) Input file: Select the start time Choose an image board Select a preset (0-13) Select the duration of the video Output file Duration: Started conversion for {0} File conversion {0} completed! File conversion {0} failed with an error! Start: Preset: File {0} is waiting for conversion 