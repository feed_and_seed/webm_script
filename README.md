# WebM Script (for KDE)
![Demonstration](demonstration.webm)
## Overview
This script makes it easy to create WebM for 4chan. It will calculate proper bitrate to achieve the best quality while respecting 4chan size limit. It will also scale you video if it's exceeding resolution limits.

Dependencies:
- Python
- KDialog
- FFmpeg
## Usage
Run script with path to file:

`./webm.py ~/Videos/video.mp4`

You can make a symbolic link to your `PATH` to use it from terminal:

```
ln -s webm.py ~/.local/bin/webm
webm ~/Videos/video.mp4
```

After making symbolic link `webm` you can add context menu if you're using **KDE**:

`ln -s make_webm.desktop ~/.local/share/kio/servicemenus/make_webm.desktop`
### Sequential conversion
By default this script will send file in a queue if there's already files being converted. To increase amount of files being converted simultaneously you should set `WEBM` environment variable, for example `WEBM=2`.